# DucoPay WooCommerce integration

This is a source for DucoPay integration for WooCommerce store.

Currently it only add "duino" currency to Woocommerce currency settings and DucoPay as payment gateway - due to low liquidity it is hard to calulating DUCO value in USD - but that might change in future.

## What it is?
DucoPay is a payment gateway to help shop owners, app developers to accept payments in Duinocoin virtual currency.    
Duino-coin is a virtual currency that can be mined pretty much on every computer and microcontrollers like Arduino or ESP8266/32   
Visit [https://duinocoin.com](https://duinocoin.com) to learn more

## How to download
To download plugin go to [Releases](https://gitlab.com/ihyoudou/ducopay-woocommerce/-/releases) page and download latest package.

## How to install
To install, download latest release, go to your Wordpress admin panel -> plugins -> add new -> Upload plugin to server,
choose it, and install.

## How to configure it
To configure plugin, you need DucoPay account.
Visit DucoPay website, login to your account, create new store

As callback URL you need to enter ```https://[yourdomain]/wc-api/ducopaywc```   
For "Thank you" URL you can enter ```https://[yourdomain]/checkout/order-received/``` - after a successful payment user will be redirected to default woocommerce 'thank you' page.


Enable plugin as payment option in Woocommerce options, then set proper shop ID and API key. You also need to change your store currency to Duino - currently there is no support for calulating USD value to DUCO, as due to low liquidity it is hard to estimate that value.
