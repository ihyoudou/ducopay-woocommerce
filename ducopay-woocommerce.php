<?php
/*
 * Plugin Name: DucoPay WooCommerce integration
 * Plugin URI: https://pay.duinocoin.com
 * Description: Take Duinocoin payments on your store.
 * Author: Issei
 * Author URI: https://issei.space
 * Version: 1.0.1
 */


/*
 * This action hook registers our PHP class as a WooCommerce payment gateway
 */

add_filter( 'woocommerce_payment_gateways', 'ducopaywc_add_gateway_class' );
function ducopaywc_add_gateway_class( $gateways ) {
	$gateways[] = 'WC_Ducopay_Gateway'; // your class name is here
	return $gateways;
}

// add duinocoin currency to list
add_filter( 'woocommerce_currencies', 'add_duinocoin_currency' );
function add_duinocoin_currency( $cw_currency ) {
     $cw_currency['DUCO'] = __( 'Duinocoin', 'woocommerce' );
     return $cw_currency;
}
add_filter('woocommerce_currency_symbol', 'add_duinocoin_currency_symbol', 10, 2);
function add_duinocoin_currency_symbol( $custom_currency_symbol, $custom_currency ) {
     switch( $custom_currency ) {
         case 'DUCO': $custom_currency_symbol = 'ᕲ'; break;
     }
     return $custom_currency_symbol;
}

/*
 * The class itself, please note that it is inside plugins_loaded action hook
 */
add_action( 'plugins_loaded', 'ducopaywc_init_gateway_class' );
function ducopaywc_init_gateway_class() {

	class WC_Ducopay_Gateway extends WC_Payment_Gateway {

 		/**
 		 * Class constructor, more about it in Step 3
 		 */
 		public function __construct() {
            $this->id = 'ducopaywc'; // payment gateway plugin ID
            $this->icon = ''; // URL of the icon that will be displayed on checkout page near your gateway name
            $this->has_fields = true; // in case you need a custom credit card form
            $this->method_title = 'DucoPay Integration';
            $this->method_description = 'Accept payments in your shop by Duinocoins'; // will be displayed on the options page

            // gateways can support subscriptions, refunds, saved payment methods,
            // but in this tutorial we begin with simple payments
            $this->supports = array(
                'products'
            );

            // Method with all the options fields
            $this->init_form_fields();

            // Load the settings.
            $this->init_settings();
            $this->title = $this->get_option( 'title' );
            $this->description = $this->get_option( 'description' );
            $this->enabled = $this->get_option( 'enabled' );
            $this->shop_id = $this->get_option( 'shop_id' );
            $this->api_key = $this->get_option( 'api_key' );

            // This action hook saves the settings
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

            // We need custom JavaScript to obtain a token
            add_action( 'wp_enqueue_scripts', array( $this, 'payment_scripts' ) );
            
            // You can also register a webhook here
            add_action( 'woocommerce_api_'.$this->id, array( $this, 'webhook' ) );
        }

		/**
 		 * Plugin options, we deal with it in Step 3 too
 		 */
 		 public function init_form_fields(){

            $this->form_fields = array(
                'enabled' => array(
                    'title'       => 'Enable/Disable',
                    'label'       => 'Enable DucoPay integration',
                    'type'        => 'checkbox',
                    'description' => '',
                    'default'     => 'no'
                ),
                'title' => array(
                    'title'       => 'Title',
                    'type'        => 'text',
                    'description' => 'This controls the title which the user sees during checkout.',
                    'default'     => 'Virtual currency (DUCO)',
                    'desc_tip'    => true,
                ),
                'description' => array(
                    'title'       => 'Description',
                    'type'        => 'textarea',
                    'description' => 'This controls the description which the user sees during checkout.',
                    'default'     => 'Pay with your Duinocoins via DucoPay payment gateway.',
                ),
                'shop_id' => array(
                    'title'       => 'ID of your shop in DucoPay',
                    'type'        => 'text'
                ),
                'api_key' => array(
                    'title'       => 'API key for your store',
                    'type'        => 'text'
                ),
            );
        }
        
		/**
		 * You will need it if you want your custom credit card form, Step 4 is about it
		 */
		public function payment_fields() {

		
				 
		}

		/*
		 * Custom CSS and JS, in most cases required only when you decided to go with a custom credit card form
		 */
	 	public function payment_scripts() {

		
	
	 	}

		/*
 		 * Fields validation, more in Step 5
		 */
		public function validate_fields() {

            return true;

		}

		/*
		 * We're processing the payments here, everything about it is in Step 5
		 */
		public function process_payment( $order_id ) {

            global $woocommerce;
 
            // we need it to get any order detailes
            $order = wc_get_order( $order_id );

            /*
              * Array with parameters for API interaction
             */
            $args = array(
                'body'=>json_encode(array(
                        'shopID'=>$this->shop_id,
                        'apikey'=>$this->api_key,
                        'amount'=>$order->get_total(),
                        'description'=>"Payment for transaction #$order_id",
                        'operationNumber'=>$order_id
                        ))
            );
         
            /*
             * Your API interaction could be built with wp_remote_post()
              */
            $response = wp_remote_post( 'https://pay.duinocoin.com/api/v1/createTransaction', $args );
         
             if( !is_wp_error( $response ) ) {
         
                 $body = json_decode( $response['body'], true );
         
                 // it could be different depending on your payment processor
                 if ( $body['success'] == true ) {
                    $uid = $body['uid'];
         
                    // Redirect to the thank you page
                    return array(
                        'result' => 'success',
                        'redirect' => "https://pay.duinocoin.com/pay/$this->shop_id/$uid"
                    );
         
                 } else {
                    wc_add_notice(  'Please try again.' . implode($body), 'error' );
                    return;
                }
         
            } else {
                wc_add_notice(  'Connection error.', 'error' );
                return;
            }
         
		
					
	 	}

		/*
		 * In case you need a webhook, like PayPal IPN etc
		 */
		public function webhook() {
            

            $receivedBody = json_decode(file_get_contents('php://input'));
            var_dump($receivedBody);
            $combined = $receivedBody->opNumber.$receivedBody->transactionID;
            $signature = base64_encode(hash_hmac('sha256', $combined, $this->api_key));
            
            
            if($signature == $receivedBody->signature){
                echo "ok";
                $order = wc_get_order( $receivedBody->opNumber );
                $order->payment_complete();
                $order->reduce_order_stock();
            
                update_option('webhook_debug', $receivedBody);
            } else {
                echo "ko";
            }
            die();
		
					
	 	}
 	}
}
